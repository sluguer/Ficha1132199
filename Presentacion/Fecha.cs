﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentacion
{
    class Fecha
    {
        private int dia;
        private int mes;
        private int ano;

        public Fecha()
        {

        }

       public  Fecha(int ano, int mes, int dia)
        {
            this.ano = ano;
            this.mes = mes;
            this.dia = dia;
        }

        private int comprovarMes(int mes)
        {
            if (mes >= 1 && mes <= 12)
            {
                return mes;
            }
            else
                return 1;
        }

        private int comprobarDia(int dia)
        {

            int[] diasMes = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
            //comproBar mes
            if (dia >= 1 && dia <= diasMes[mes])
            {
                return dia;
            }
            // comproBar dias del mes febrero
            if (mes == 2 && dia == 29 && (ano % 400 == 0 || ano % 4 == 0 && ano % 100 != 0))
            {
                return dia;  //           si es 29 febrero
            }
            return 1;
        }

        public override string ToString()
        {
            return String.Format(" {0:d}, {0:d}, {0:d}", ano, mes, dia);
        }

    }
}
