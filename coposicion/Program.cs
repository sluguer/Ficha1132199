﻿using composicion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace coposicion 
{
    class Program
    {
        static void Main(string[] args)
        {

            Fecha fechaNacimiento = new Fecha(1982,06,28);
            Fecha fechaContratacion = new Fecha(2016,02,12);
            Empleado oEmpleado = new Empleado("sluguer", "arana",fechaNacimiento,fechaContratacion);
            Console.Write(oEmpleado); // si quiero se puede dejar sin tostring();
            Console.ReadKey();  
            
            
        }
    }
}
