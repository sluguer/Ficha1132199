﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace composicion
{
    class Empleado
    {

        private string nombres;
        private string apellidos;
        private Fecha fechaNacimiento;
        private Fecha fechaContratacion;

        public Empleado(string nombres, string apellidos, Fecha fechaNacimiento, Fecha fechaContratacion)
        {
            this.nombres = nombres;
            this.apellidos = apellidos;
            this.fechaNacimiento = fechaNacimiento;
            this.fechaContratacion = fechaContratacion;
        }

        public override string ToString()
        {
            return   "\tnombres: " + nombres + "\n"
                   + "\tapellidos: " + apellidos + "\n"
                   + "\tF.Nacimiento: " + fechaNacimiento + "\n"
                   + "\tF.Contratacion: " + fechaContratacion + "\n";
        }


    }
}
